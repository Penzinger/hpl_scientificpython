# -*- coding: utf-8 -*-
"""
Created on Sat Sep  1 01:02:03 2018

@author: julian
"""

'''
Ex. 2.1: Make a Fahrenheit-Celsius conversion table.
'''

# Creating list of Fahrenheit degrees betw. 0 and 100 °F
Fdegrees = [i for i in range(0,101,10)]

# Converting values in the Fahrenheit degrees list to Celsius degrees.
Cdegrees = [(F-32.)*(5/9.) for F in Fdegrees]

print("Fahrenheit - Celsius Conversion table")
print('---------------------------')
print('Fahrenheit      Celsius')

for F, C in zip(Fdegrees, Cdegrees):
    print('{0:8.1f}°F  {1:8.1f}°C'.format(F, C))

print('---------------------------')
